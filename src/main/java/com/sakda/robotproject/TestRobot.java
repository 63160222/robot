/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.robotproject;

/**
 *
 * @author MSI GAMING
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0,10,20,100);
        System.out.println(robot);
        robot.walk('E');
        System.out.println(robot);
        robot.walk(2);
        System.out.println(robot);
        robot.walk('N', 10);
        System.out.println(robot);
        robot.walk('S', 20);
        System.out.println(robot);
        robot.walk('E',7);
        System.out.println(robot);
    }
}
